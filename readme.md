# Тестовое задание на вакансию «Front-end разработчик»

Решите один (или несколько) из предложенных кейсов на выбор, что бы мы смогли оценить ваш уровень и опыт.

**Шаблоны** должны быть выполнены в `Pug`, стили на `Stylus`


## 0) Приложение на `Nuxt.js` с авторизацией, содержит разделы и страницы доступные пользователям с разными ролями доступа. 
`Low level`

back-end писать не обязательно, можно реализовать всё средствами `Nuxt`, либо замокать

**Например:**

Роли: `admin`, `manager`, `user`

Разделы:
- Страница авторизации
- Админка 
    - Список пользователей (admin)
    - Управление каталогом (admin, manager) CRUD, если посчитатет нужным можно добавить категории, теги, etc.
- Публичный каталог товаров (admin, manager, user): READ
- Профиль (other auth)


## 1) Компонент таблицы на Vue.js.

`Middle level`

**Требования:**

- [ ] Реализовать возможность изменять представление внутри ячеек, т.е. через слот передавать специфический шаблон контента. Например вывод картинки, форматирование данных ячейки;
- [ ] В **шапке таблицы** реализовать возможность изменять представление внутри ячеек, т.е. через слот передавать специфический шаблон контента. Например вывод картинки, форматирование данных ячейки;
- [ ] Стили таблицы могут быть настроены как глобально, так и локально на уровне вызова конкретного экземпляра компонента (предложите свой вариант реализации темы для таблицы).
- [ ] Данные для таблицы могут быть замоканы, либо переданы из стороннего источника;

При употреблении компонента должно получится что-то вроде

```vue
<script>
const collection = [
    {
        name: 'John Dow',
        job: 'Developer',
        dob: '10.12.1988',
        photo: 'http://lorempixel.com/output/people-q-c-200-200-6.jpg'
    },
    {
        name: 'Dzhigurda',
        job: 'Clown',
        dob: '24.03.1962',
        photo: 'http://lorempixel.com/output/people-q-c-200-200-6.jpg'
    }
]

export default {
    name: 'TableExampleUsage',
    data: () => { collection }
}
</script>
<template lang="pug">
    table-component(:data="collection")
        table-column-component(field="name")
            template(#label)
                .color(:style="{ color: 'red' }")
                    .color__label Имя                
        table-column-component(field="photo", label="Фотогафия")
            template(#default="{ row: { photo } }") 
                img(:src="photo")
        table-column-component(field="dob", label="День рождения")
        table-column-component(field="job", label="Должность")
</template>

```

## 2) Компонент `date-picker` с календариком 
`Middle level`

Требования:
- [ ] Формирование сетки календаря на месяц;
- [ ] Возможность изменить год, месяц, установить значение дня конкретной даты;
- [ ] Отображение текущей даты (сегодняшнего дня);
- [ ] Возможность ввода/вывода значения через `v-model`;
- [ ] Если справитесь, то реализовать так же ручной ввод даты строкой;

При употреблении компонента должно получится что-то вроде:

```vue
<tamplate lang="pug">
    date-picker-component(v-model="dateAttribute")
</tamplate>    
```

### Результат

Результат работы можете предоставить через merge-request к этому репозиторию;

Плюсом будет описание своего компонента в Storybook, наличие комментариев в коде, ваши комментарии по поводу решённых кейсов.
